package com.uglylabs.logbrowser.gui

import org.junit.Test
import org.junit.Assert._
import com.uglylabs.logbrowser.core.Log
import java.nio.file.Paths
import java.nio.charset.Charset
import com.uglylabs.logbrowser.core.ParseConfig
import org.easymock.EasyMock._
import scala.concurrent.Await

class MainViewPresenterImplTest {
	@Test
	def testTextSearch() {
		// setup mock 
		val view = createNiceMock(classOf[MainView])

		// search forward part
		expect(view.scrollTo(0))
		expect(view.scrollTo(1))
		expect(view.scrollTo(2))
		expect(view.scrollTo(0))
		
		replay(view)
		
		// main test 
		val presenter = new MainViewPresenterImpl(view)
		
		presenter.logFileOpt = Some(Paths.get("src/test/resources/simple.log"))
		presenter.reindex(ParseConfig("%d %-5p [%c] (%t) %s%n", Charset.forName("utf-8")))
		
		// re-indexing is now running in a separate thread, so wait for a while
		Stream.from(0).takeWhile(_ => presenter.logOpt.isEmpty).map{ i =>
			Thread.sleep(1000)
			if(i > 5) 
				fail("Wait for value too long")
		}.last
		
		println("Presenter log: " + presenter.logOpt)
		
		assertTrue(presenter.logOpt.isDefined)
		assertEquals(3, presenter.logOpt.get.lines.size)
		
		presenter.searchText(Some("a"), true, 0)
		presenter.searchText(Some("a"), true, +1)
		presenter.searchText(Some("a"), true, +2)
		presenter.searchText(Some("a"), true, +3)
		
		verify(view)
	}
}
package com.uglylabs.logbrowser.core

import org.junit.Test
import org.junit.Assert._
import java.io.StringReader
import java.util.regex.Pattern
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser
import scala.io.Source

class LogLineIteratorTest {
	import scala.language.implicitConversions
	import scala.language.reflectiveCalls
	
	implicit def str2newlinehelper(s: String) = new {
		def showNewlines = s.replaceAll("\n", "\\\\n").replaceAll("\r", "\\\\r")
	}
	
	@Test
	def testEmpty() {
		val logParser = LogbackPatternParser("""%s%n""")
		println(logParser.createPattern().showNewlines)
		
		val reader = Source.fromString("").getLines
		
		val it = LogLineIterator(reader, logParser)
		
		println(logParser.columnTerms)
		assertFalse(it.hasNext())
	}
	
	@Test
	def testSingleline() {
		val logParser = LogbackPatternParser("""%s%n""")
		println(logParser.createPattern().showNewlines)
		
		val reader = Source.fromString("""log line 1
				|log line 2""".stripMargin).getLines
		
		val it = LogLineIterator(reader, logParser)
		
		println(logParser.columnTerms)
		assertTrue(it.hasNext())
		val line1 = it.next()
		println(line1)

		assertTrue(it.hasNext())
		val line2 = it.next()
		println(line2)

		assertFalse(it.hasNext())
		
		assertEquals("log line 1", line1.items(0))
		assertTrue(line1.tail.isEmpty)
		assertEquals("log line 2", line2.items(0))
		assertTrue(line2.tail.isEmpty)
	}
	
	@Test
	def testMultiline() {
		val logParser = LogbackPatternParser("""%s%n%s%n""")
		println(logParser.createPattern().showNewlines)
		
		val reader = Source.fromString("""log line 1
				|continue log line 1
				|log line 2
				|continue log line 2""".stripMargin).getLines
		
		val it = LogLineIterator(reader, logParser)
		
		println(logParser.columnTerms)
		assertTrue(it.hasNext())
		val line1 = it.next()
		println(line1)
		
		assertTrue(it.hasNext())
		val line2 = it.next()
		println(line2)
		
		assertFalse(it.hasNext())
		
		assertEquals("log line 1", line1.items(0))
		assertEquals("continue log line 1", line1.items(1))
		assertTrue(line1.tail.isEmpty)
		assertEquals("log line 2", line2.items(0))
		assertEquals("continue log line 2", line2.items(1))
		assertTrue(line2.tail.isEmpty)
	}
	
	@Test
	def testMultilineIncompleteSimple() {
		val logParser = LogbackPatternParser("""%s%n%s%n""")
		println(logParser.createPattern().showNewlines)
		
		val reader = Source.fromString("log line 1\n").getLines
		
		val it = LogLineIterator(reader, logParser)
		
		println(logParser.columnTerms)
		assertTrue(it.hasNext)
		val line1 = it.next()
		println(line1)
		assertFalse(it.hasNext)
		assertEquals(" -- ", line1.items(0))
		assertEquals(" -- ", line1.items(1))
		assertEquals("log line 1\n", line1.tail.head)
	}
	
	@Test
	def testMultilineIncomplete() {
		val logParser = LogbackPatternParser("""%s%n%s%n""")
		println(logParser.createPattern().showNewlines)
		
		val reader = Source.fromString("""log line 1
				|continue log line 1
				|log line 2""".stripMargin).getLines
		
		val it = LogLineIterator(reader, logParser)
		
		println(logParser.columnTerms)
		assertTrue(it.hasNext)
		val line1 = it.next()
		println(line1)
		assertFalse(it.hasNext)
		assertEquals("log line 1", line1.items(0))
		assertEquals("continue log line 1", line1.items(1))
		assertEquals("log line 2\n", line1.tail.head)
	}
	
	@Test
	def testMultilinePattern() {
		val p = Pattern.compile("(.+)(\r|\n|\r\n)(.+)", Pattern.MULTILINE)
		val m = p.matcher("abc\ncde")
		assertTrue(m.matches())
		
		println(m.group(0))
		println(m.group(1))
	}
}
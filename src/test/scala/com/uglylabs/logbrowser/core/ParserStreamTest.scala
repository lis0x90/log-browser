package com.uglylabs.logbrowser.core

import org.junit.Test
import org.junit.Assert._
import java.nio.file.Files
import java.nio.file.StandardOpenOption
import java.nio.file.Paths
import java.io.Closeable
import java.nio.charset.Charset
import ru.kamis.utils.commons.scala.CloseableHelper._
import ru.kamis.utils.commons.scala.Timing
import java.util.concurrent.atomic.AtomicInteger
import java.nio.charset.StandardCharsets
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser
import scala.io.Source
import scala.io.Codec

class ParserStreamTest {
	val logParser = LogbackPatternParser("""%d %-5p [%c] (%t) %s%E""")
	implicit val charset = Codec.UTF8
	
	@Test
	def testStream1() {
		val testFile = Paths.get("src/test/resources/simple.log")
		
		using(Source.fromFile(testFile.toFile)) { reader =>
			val logLines = LogLineIterator(reader.getLines, logParser).toList
			
			assertEquals(3, logLines.size)
			assertEquals(2, logLines(1).tail.size)
		}
	}
	
	@Test
	def testStream2() {
		val testFile = Paths.get("src/test/resources/missed-tail.log")
		using(Source.fromFile(testFile.toFile)) { reader =>
			val lines = LogLineIterator(reader.getLines, logParser).toList
			
			println(lines.mkString("\n"))
			assertEquals(4, lines.size)
			assertEquals(" -- ", lines(0).items(0))
		}
	}
}

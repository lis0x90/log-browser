package com.uglylabs.logbrowser.core

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import org.junit.Test
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser
import ru.kamis.utils.commons.scala.CloseableHelper.using
import ru.kamis.utils.commons.scala.Timing
import scala.io.Source
import scala.io.Codec
import org.junit.Ignore


/**
 * @author s.lisovoy
 */
class PerformanceTest {
	val logParser = LogbackPatternParser("""%d %-5p [%c] (%t) %s%n""")
	implicit val _ = Codec.UTF8
	
	@Test
	@Ignore
	def testRegexParser() {
//		System.in.read()
		println("Start...")
		val lines = Timing.measure("Time spent: %s") {
			val testFile = Paths.get("""H:\temp\mano-analysis\server.log""")
			using(Source.fromFile(testFile.toFile)) { reader =>
				LogLineIterator(reader.getLines, logParser)
					.zipWithIndex
					.map { case (data, count) => 
						if (count % 1000 == 0) println(s"Count: $count")
						data
					}.take(100000)
					.toSeq
					.last
			}
		}
		//println(lines.size)
//		System.in.read()
//		System.in.read()
	}
	
	@Test
	@Ignore
	def testStumble() {
		import ParserUtils._
		val config = ParseConfig("%s%n", StandardCharsets.UTF_8)
		implicit val _ = config.charset
		val logfile = Paths.get("""H:\temp\server.log""")
		withLogIterator(logfile, config, Some(100)) { (parsers, logIterator) =>
				println(logIterator)
				
				while(logIterator.hasNext()) {
					println(logIterator.next())
				}
			
				val it = logIterator.toList
				val logViewOpt = Log(parsers, it)
				println(logViewOpt)
			}
	}
}
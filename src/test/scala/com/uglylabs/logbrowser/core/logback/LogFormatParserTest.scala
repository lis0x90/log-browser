package com.uglylabs.logbrowser.core.logback

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

import com.uglylabs.logbrowser.core.Align
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser.alignTerm
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser.anyCharTerm
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser.loggerTerm
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser.parseAll

class LogFormatParserTest {
	@Test
	def testAlignTerm1() {
		val p = parseAll(alignTerm, "1.")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(Align(Some(1), None), p.get)
	}
	
	@Test
	def testAlignTerm2() {
		val p = parseAll(alignTerm, "1")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(Align(Some(1), None), p.get)
	}
	
	@Test
	def testAlignTerm3() {
		val p = parseAll(alignTerm, ".12")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(Align(None, Some(12)), p.get)
	}
	
	@Test
	def testAlignTermFull() {
		val p = parseAll(alignTerm, "-10.12")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(Align(Some(-10), Some(12)), p.get)
	}
	
	@Test
	def testLogger1() {
		val p = parseAll(loggerTerm, "%-10.12c{1}")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(LoggerTerm(Some(Align(Some(-10), Some(12))), Some(1)), p.get)
	}
	
	@Test
	def testLogger2() {
		val p = parseAll(loggerTerm, "%.-30c{1}")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(LoggerTerm(Some(Align(None, Some(-30))), Some(1)), p.get)
	}
	
	@Test
	def testLogger3() {
		val p = parseAll(loggerTerm, "%20c{1}")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(LoggerTerm(Some(Align(Some(20), None)), Some(1)), p.get)
	}
	
	@Test
	def testLogger4() {
		val p = parseAll(loggerTerm, "%c{1}")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(LoggerTerm(None, Some(1)), p.get)
	}
	
	@Test
	def testLogger5() {
		val p = parseAll(loggerTerm, "%c")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(LoggerTerm(None, None), p.get)
	}
	
	@Test
	def testAnyCharTerm() {
		val p = parseAll(anyCharTerm, "r")
		
		println(p)
		assertTrue(p.successful)
		assertEquals(CharTerm("r"), p.get)
	}
	
	
	@Test
	def testLoggerLine() {
		val p = LogbackPatternParser("%-5p [%t]: %m%n")
				
		println(p)
//		assertTrue(p.successful)
//		assertEquals(CharTerm("r"), p.get)
	}
	
	@Test
	def testLogbackParser() {
		val logParser = LogbackPatternParser("""%d{yyyy-MM-dd HH:mm:ss,SSS} %-5p [%c] (%t) %s%E""")
		println(logParser.createPattern())
		
		val input  = """2014-12-02 00:00:00,092 INFO  [org.jboss.as.connector.deployers.jdbc] (MSC service thread 1-4) JBAS010418: Stopped Driver service with driver-name = h2""" + "\n"
		val result = logParser.parse(input)
		println(logParser.columnTerms)
		println(result)

		assertTrue(result.isLeft)
		val chunks = result.left.get
		assertEquals("2014-12-02 00:00:00,092", chunks(0))
		assertEquals("INFO", chunks(1))
		assertEquals("org.jboss.as.connector.deployers.jdbc", chunks(2))
		assertEquals("MSC service thread 1-4", chunks(3))
		assertEquals("JBAS010418: Stopped Driver service with driver-name = h2", chunks(4))
	}
	
	
	/*
	@Test
	def testLogger2() {
		val p = parseAll(loggerTerm, "%lo")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(LoggerTerm(None), p.get)
	}
	
	@Test
	def testDateFormat1() {
		val p = parseAll(dateTerm, "%d")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(DateTerm(), p.get)
	}
	
	@Test
	def testDateFormat2() {
		val p = parseAll(dateTerm, "%d{HH:mm:ss.SSS}")
	
		println(p)
		assertTrue(p.successful)
		assertEquals(DateTerm(), p.get)
	}*/
}
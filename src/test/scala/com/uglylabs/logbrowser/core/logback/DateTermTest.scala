package com.uglylabs.logbrowser.core.logback

import org.junit.Assert._
import org.junit.Test
import SimpleDateFormatParser._
import date._
import java.text.SimpleDateFormat
import java.util.Calendar
import org.junit.Ignore

/**
 * @author s.lisovoy
 */
class DateTermTest {
	@Test
	def testYear() = {
		val result = parseAll(year, "y").get
		assertEquals(YearTerm("y"), result)
		assertEquals("""\d{4}""", result.pattern)
	}
	
	@Test
	def testYear2() = {
		val result = parseAll(year, "yy").get
		assertEquals(YearTerm("yy"), result)
		assertEquals("""\d{2}""", result.pattern)
		
	}

	@Test
	def testYear4() = {
		val result = parseAll(year, "yyyy").get
		assertEquals(YearTerm("yyyy"), result)
	}
	
	@Test
	def testMonth1() = {
		val result = parseAll(month, "M").get
		assertEquals(MonthTerm("M"), result)
	}

	@Test
	def testMostUsedPattern() {
		val format = "yyyy-MM-dd HH:mm:ss,SSS"
		val result = parseAll(allTerms, format)
		println(s"Parsed: $result")
		
		val expected = List(YearTerm("yyyy"),
			CharsTerm("-"), 
			MonthTerm("MM"), 
			CharsTerm("-"), 
			DayInMonthTerm("dd"), 
			CharsTerm(" "), 
			HourInDayTerm("HH"), 
			CharsTerm(":"), 
			MinuteInHourTerm("mm"), 
			CharsTerm(":"), 
			SecondInMinuteTerm("ss"), 
			CharsTerm(","), 
			MillisecondTerm("SSS")
		)
		
		assertEquals(expected, result.get)
	}
	
	@Test
	def testMostUsedPatternRegeExp() {
		val format = "yyyy-MM-dd HH:mm:ss,SSS"
		val pattern = SimpleDateFormatParser.createPattern(format)
		println(s"Parsed: $pattern")
		
		assertEquals("""\d{4}\Q-\E\d{2}\Q-\E\d{2}\Q \E\d{2}\Q:\E\d{2}\Q:\E\d{2}\Q,\E\d{3}""", pattern)
	}
	
	@Test
	def testStringInPattern() {
		val format = "'hello at' HH 'hour'"
		val result = parseAll(allTerms, format)
		println(s"Parsed: $result")
		
		val expected = List(CharsTerm("'hello at'"), 
			CharsTerm(" "), 
			HourInDayTerm("HH"), 
			CharsTerm(" "), 
			CharsTerm("'hour'") 
		)
		assertEquals(expected, result.get)
	}

	@Test
	def testDateTermCreateParser() {
		
	}
	
	@Test
//	@Ignore
	def testNativeFormatter() {
		println("---------------------------------------")
		val format = "MMMM"
		val sdf = new SimpleDateFormat(format)
		val d = Calendar.getInstance()
		d.set(2015, 1, 16, 1, 1, 1)
		d.set(Calendar.MILLISECOND, 5)

		println("Date " + d.getTime)
		val formatedString = sdf.format(d.getTime)
		println(s"Formatted: $formatedString")
		println("---------------------------------------")
	}
}
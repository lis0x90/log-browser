package com.uglylabs.logbrowser.core.logback

import org.junit.Test
import org.junit.Assert._
import com.uglylabs.logbrowser.core.Align

class LevelTermTest {
	@Test
	def testAlign1() {
		val t = LevelTerm(Some(Align(Some(5), None)))
		assertEquals("( INFO| WARN|DEBUG|ERROR|FATAL|TRACE)", t.createPattern())
	}
	
	@Test
	def testAlign2() {
		val t = LevelTerm(Some(Align(Some(-5), None)))
		assertEquals("(DEBUG|ERROR|FATAL|INFO |TRACE|WARN )", t.createPattern())
	}
	
	@Test
	def testAlign3() {
		val t = LevelTerm(Some(Align(None, Some(4))))
		assertEquals("(DEBU|ERRO|FATA|INFO|TRAC|WARN)", t.createPattern())
	}
}
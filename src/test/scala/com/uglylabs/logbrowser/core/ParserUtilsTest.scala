package com.uglylabs.logbrowser.core

import org.junit.Test
import org.junit.Assert._
import java.nio.file.Paths
import java.nio.charset.Charset
import scala.io.Source

class ParserUtilsTest {
	val logfile = Paths.get("src/test/resources/parser/small.log")
	val config = ParseConfig("%s%n", Charset.forName("utf-8"))
	
	@Test
	def testLimitedReadingCrop() {
		ParserUtils.withLogIterator(logfile, config, Some(6)) { (parser, it) =>
			val lines = it.toList
			
			println(lines.mkString("\n"))
			assertEquals(3, lines.size)
		}
	}
	
	@Test
	def testLimitedReadingAll() {
		ParserUtils.withLogIterator(logfile, config, Some(100)) { (parser, it) =>
			val lines = it.toList
			
			println(lines.mkString("\n"))
			assertEquals(3, lines.size)
		}
	}
}
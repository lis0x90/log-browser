package com.uglylabs.logbrowser.gui

import java.net.URL
import java.nio.file.Paths
import java.util.ResourceBundle
import java.util.Timer
import java.util.TimerTask
import java.util.UUID
import java.util.regex.Pattern

import org.slf4j.LoggerFactory

import com.uglylabs.logbrowser.core.Log
import com.uglylabs.logbrowser.core.LogLevel
import com.uglylabs.logbrowser.core.LogLine
import com.uglylabs.logbrowser.core.ParseConfig
import com.uglylabs.logbrowser.core.logback.LevelTerm
import com.uglylabs.logbrowser.gui.MainView.SearchStatus
import com.uglylabs.logbrowser.gui.dialogs.ReconfigureDialog

import StatusLineCell._
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.canvas.Canvas
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.scene.control.SplitPane
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import javafx.scene.paint.Color
import javafx.scene.web.WebView
import javafx.stage.FileChooser
import javafx.stage.Stage
import javafx.util.Callback
import ru.kamis.utils.commons.scala.FileUtils

object MainViewImpl {
	private val log = LoggerFactory.getLogger(getClass)
}

class MainViewImpl extends Initializable with MainView {
	import LogLevel._
	import MainViewImpl._
	import FXUtils.{
		cellValueFactory => fxCellValueFactory,
		onChange => fxOnChange
	}

	@FXML var navigateMenuItem: Menu = _
	@FXML var splitPane: SplitPane = _

	@FXML var logTableView: TableView[LogLine] = _
	@FXML var detailsWebView: WebView = _

	@FXML var textSearchTextField: TextField = _
	@FXML var textSearchPrevButton: Button = _
	@FXML var textSearchNextButton: Button = _

	@FXML var reconfigureMenuItem: MenuItem = _

	@FXML var textSearchPrevMenuItem: MenuItem = _
	@FXML var textSearchNextMenuItem: MenuItem = _

	@FXML var spreadsPane: HBox = _
	@FXML var levelsCanvas: Canvas = _

	@FXML var statusLineFileLabel: Label = _
	@FXML var statusLineTotalLinesLabel: Label = _
	@FXML var statusLineCurrentLineLabel: Label = _

	var logViewOpt: Option[Log] = None

	// detail update deffered task
	var updateDetailsViewTask = new Timer()

	// search text occurrence markers to mark found text 
	// before html char escaping and restore after
	val markerStart = "__" + UUID.randomUUID().toString + "__"
	val markerEnd = "__" + UUID.randomUUID().toString + "__"

	override def initialize(url: URL, bundle: ResourceBundle) {
		log.info("Initialize main window")
		checkInjectedValues()
		splitPane.setVisible(false)

		val engine = detailsWebView.getEngine()
		require(engine != null)
		engine.setUserStyleSheetLocation(getClass.getResource("/details.css").toExternalForm());
	}

	override def setAppTitle(subtitle: Option[String]) = {
		val title = "Log Browser" + subtitle.map(" :: " + _).getOrElse("")
		stage.setTitle(title)
	}

	override def stage_=(value: Stage): Unit = {
		super.stage_=(value)

		setAppTitle(None)
		bindEventHandlers()
	}

	private def bindEventHandlers() = {
		stage.getScene.getAccelerators().put(new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN), new Runnable {
			override def run() = textSearchTextField.requestFocus()
		})

		textSearchTextField.textProperty().addListener(FXUtils.onChange[String] { (_, _, next) =>
			presenter.searchText(searchText, true, selectionIndex)
		})

		logTableView.getSelectionModel().selectedItemProperty().addListener(FXUtils.onChange[LogLine] { (_, _, _) =>
			updateDetailView()
		})

		logTableView.getSelectionModel().selectedIndexProperty().addListener(FXUtils.onChange[Number] { (_, _, next) =>
			updateStatusLine(StatusLineCell.CURRENT_LINE, s"Current pos: ${next.intValue + 1}")
		})

		spreadsPane.heightProperty().addListener(FXUtils.onChange[Number] { (pane, _, value) =>
			levelsCanvas.setHeight(value.doubleValue())

			drawLevelsSpread()
		})

		levelsCanvas.setOnMouseClicked(new EventHandler[MouseEvent] {
			override def handle(ev: MouseEvent) {
				val pos = ((ev.getY() / levelsCanvas.getHeight()) * logTableView.getItems.size).toInt
				scrollTo(pos)
			}
		})

		// restore splitter position 
		UserPref.get("splitPaneDividerPosition").map { value =>
			splitPane.setDividerPositions(value.toDouble)
		}
		splitPane.getDividers().get(0).positionProperty().addListener(FXUtils.onChange[Number] { (pane, _, value) =>
			UserPref.set("splitPaneDividerPosition", value.toString)
		})
	}

	private def checkInjectedValues() = {
		require(navigateMenuItem != null)
		require(splitPane != null)
		require(logTableView != null)
		require(textSearchTextField != null)
		require(detailsWebView != null)
		require(spreadsPane != null)
		require(statusLineFileLabel != null)
		require(statusLineTotalLinesLabel != null)
		require(statusLineCurrentLineLabel != null)

		require(textSearchPrevButton != null)
		require(textSearchNextButton != null)
		require(textSearchPrevMenuItem != null)
		require(textSearchNextMenuItem != null)

		require(levelsCanvas != null)
	}

	private def searchText =
		if (textSearchTextField.getText().isEmpty()) None else Some(textSearchTextField.getText())

	private def selectionIndex =
		logTableView.getSelectionModel().getSelectedIndex()

	def onOpen() = {
		val fileChooser = new FileChooser()
		val logFilter = new FileChooser.ExtensionFilter("Log files (*.log)", "*.log")
		val allFilter = new FileChooser.ExtensionFilter("All files (*.*)", "*.*")
		fileChooser.getExtensionFilters().add(logFilter)
		fileChooser.getExtensionFilters().add(allFilter)

		UserPref.get("lastOpenLogFile").map { path =>
			fileChooser.setInitialDirectory(FileUtils.nearestPath(Paths.get(path).getParent).toFile)
		}

		Option(fileChooser.showOpenDialog(stage)).map { file =>
			presenter.open(file.toPath())
			UserPref.set("lastOpenLogFile", file.getAbsolutePath())
		}
	}

	override def enableOpenedFileControls() = {
		navigateMenuItem.setDisable(false)
		splitPane.setVisible(true)
		reconfigureMenuItem.setDisable(false)
	}

	override def showReconfigure(config: ParseConfig) = {
		presenter.logFileOpt.map { filepath =>
			ReconfigureDialog.show(filepath, config)(stage).map { config =>
				presenter.reindex(config)
			}
		}
	}

	// UI handlers 
	def onReconfigure() = presenter.reconfigure()

	def onNavigateToFirst() = scrollTo(0)
	def onNavigateToLast() = scrollTo(logTableView.getItems().size - 1)

	def onSearchNext() = presenter.searchText(searchText, true, selectionIndex + 1)
	def onSearchPrev() = presenter.searchText(searchText, false, selectionIndex - 1)

	def onFindPrevWarn() = presenter.scrollToLevel(WARN, false, selectionIndex)
	def onFindNextWarn() = presenter.scrollToLevel(WARN, true, selectionIndex)
	def onFindPrevError() = presenter.scrollToLevel(ERROR, false, selectionIndex)
	def onFindNextError() = presenter.scrollToLevel(ERROR, true, selectionIndex)

	def onQuit() = {
		presenter.quit()
		// TODO any questions before quit?
		stage.close()
	}

	override def cleanTable() {
		logTableView.getColumns().clear()
	}

	import StatusLineCell._
	override def updateStatusLine(part: StatusLineCell, value: String) {
		part match {
			case FILEPATH =>
				statusLineFileLabel.setText(value)
			case TOTAL_LINES =>
				statusLineTotalLinesLabel.setText(value)
			case CURRENT_LINE =>
				statusLineCurrentLineLabel.setText(value)
			case _ =>
				throw new IllegalArgumentException("Unknown status line part: " + part)
		}
	}

	override def createTable(log: Log) = {
		logViewOpt = Some(log)

		// add rest option columns 
		log.columns.zipWithIndex.foreach { case (term, pos) =>
				val tableColumn = new TableColumn[LogLine, String]()
				tableColumn.setText(term.columnName)

				if (term.isInstanceOf[LevelTerm]) {
					tableColumn.setCellFactory(levelCellFactory)
				}

				tableColumn.setCellValueFactory(fxCellValueFactory[LogLine, String] { cell =>
					val cellText = cell.getValue().items(pos)

					// to avoid scrolling lags we truncate all long strings
					// TODO: move truncate size to application properties
					val TABLE_STRING_MAX_LENGTH = 200
					val sizedText = if (cellText.length > TABLE_STRING_MAX_LENGTH) {
						cellText.substring(0, TABLE_STRING_MAX_LENGTH) + "... (cut by log-browser)"
					} else {
						cellText
					}

					new SimpleStringProperty(sizedText)
				})

				logTableView.getColumns().add(tableColumn)
		}

		// add interval column if it exists 
		log.durationColumn.map {
			case (pos, data) =>
				val tableColumn = new TableColumn[LogLine, String]()
				tableColumn.setText("Duration")
				tableColumn.setVisible(false)

				tableColumn.setCellValueFactory(fxCellValueFactory[LogLine, String] { cell =>
					val logLine = cell.getValue()
					new SimpleStringProperty(
						if (logLine.index < log.lines.size - 1) {
							data(logLine.index) + "ms"
						} else {
							"--"
						})
				})
				
				logTableView.getColumns().add(pos + 1, tableColumn)
		}

		val listWrapper = new LogObservableWrapper(log.lines)
		logTableView.setItems(null) // remove previous list if it exists
		logTableView.setItems(listWrapper)

		// create spreads
		drawLevelsSpread()

		// select first line if it exists
		if (!log.lines.isEmpty) {
			FXUtils.insidePlatform {
				logTableView.getSelectionModel.select(0)
				logTableView.getFocusModel.focus(0)
			}
		}
	}

	def scrollTo(pos: Int) = {
		log.info(s"Scroll to: $pos")
		logTableView.scrollTo(pos)
		logTableView.getSelectionModel().select(pos)
		logTableView.getFocusModel().focus(pos)
	}

	private val updateDetailsViewLock = new Object()
	override def updateDetailView() = updateDetailsViewLock.synchronized {
		updateDetailsViewTask.cancel()
		
		updateDetailsViewTask = new Timer(true)
		updateDetailsViewTask.schedule(new TimerTask {
			override def run() {
				val logLineOpt = Option(logTableView.getSelectionModel.getSelectedItem())

				val text = logLineOpt.map { logLine =>
					logViewOpt.get.restoreLogText(logLine)
				}.getOrElse {
					""
				}

				val highlited = searchText.map { t =>
					val searchPattern = s"(" + Pattern.quote(t) + ")"
					text.replaceAll(searchPattern, s"$markerStart$$1$markerEnd")
				}.getOrElse(text)

				val html = highlited
					.replaceAll("&", "&amp;")
					.replaceAll("<", "&lt;")
					.replaceAll(">", "&gt;")
					.replaceAll(markerStart, "<b>")
					.replaceAll(markerEnd, "</b>")

				FXUtils.insidePlatform {
					detailsWebView.getEngine().loadContent(s"""<body>$html</body>""")
				}
			}
		}, 250)
	}

	/**
	 * Row coloring cell
	 */
	private val levelCellFactory =
		new Callback[TableColumn[LogLine, String], TableCell[LogLine, String]]() {
			override def call(col: TableColumn[LogLine, String]): TableCell[LogLine, String] = {
				return new TableCell[LogLine, String]() {
					override protected def updateItem(level: String, empty: Boolean): Unit = {
						super.updateItem(level, empty)
						Option(level).map { level =>
							getStyleClass().removeAll(LogLevel.values.map(_.toString).toSeq: _*)
							if (!empty) {
								setText(level)
								getStyleClass().add(level)
							} else {
								setText(null)
							}
						}
					}
				}
			}
		}

	def drawLevelsSpread() = {
		val width = levelsCanvas.getWidth()
		val height = levelsCanvas.getHeight()
		val markerMinHeight = 2

		// TODO: externalize color and bind its to cell colors
		def color = Map(TRACE -> Color.web("#ffffff"),
			DEBUG -> Color.web("#eeeeee"),
			INFO -> Color.web("#cae6ff"),
			WARN -> Color.web("#f48840"),
			ERROR -> Color.web("#ff4242"),
			FATAL -> Color.web("#ff4242"))

		// calc spread data
		var lastPos: Int = -1
		var lastLevelValue: Int = -1

		val ctx = levelsCanvas.getGraphicsContext2D()
		ctx.clearRect(0, 0, width, height)

		logViewOpt.map { logView =>
			logView.levelColumnIndexOpt.map { levelColPos =>
				val totalLines = logView.lines.size

				for (i <- 0 until totalLines) {
					val y0 = (i * height / totalLines).toInt
					val y1 = Math.max(((i + 1) * height / totalLines), y0 + markerMinHeight).toInt

					logView.getLineLevel(i).map { lineLevel =>
						if (lastPos >= y1) {
							if (lineLevel.id > lastLevelValue) {
								lastLevelValue = lineLevel.id
								ctx.setFill(color(lineLevel))
								ctx.fillRect(0, y0, width, y1)
							}
						} else {
							lastLevelValue = lineLevel.id
							ctx.setFill(color(lineLevel))
							ctx.fillRect(0, y0, width, y1)
						}

						lastPos = y1
					}.getOrElse {
						log.error("Line contain no level")
					}
				}
			}
		}
	}

	override def showTextSearchStatus(found: SearchStatus.SearchStatus) = {

		val disableButtons = found match {
			case SearchStatus.UNKNOWN =>
				StyleUtils.cleanTextFieldStatus(textSearchTextField)
				true

			case SearchStatus.NOT_FOUND =>
				StyleUtils.setTextFieldStatus(textSearchTextField, false)
				true

			case SearchStatus.FOUND =>
				StyleUtils.setTextFieldStatus(textSearchTextField, true)
				false
		}

		textSearchPrevButton.setDisable(disableButtons)
		textSearchNextButton.setDisable(disableButtons)
		textSearchPrevMenuItem.setDisable(disableButtons)
		textSearchNextMenuItem.setDisable(disableButtons)
	}
}
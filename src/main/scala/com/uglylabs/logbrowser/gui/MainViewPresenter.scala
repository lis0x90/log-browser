package com.uglylabs.logbrowser.gui

import java.nio.file.Path

import com.uglylabs.logbrowser.core.Log
import com.uglylabs.logbrowser.core.LogLevel
import com.uglylabs.logbrowser.core.ParseConfig

trait MainViewPresenter {
	val view: MainView
	
	var logFileOpt: Option[Path]
	
	/**
	 * Open log file 
	 */
	def open(logFile: Path, configOpt: Option[ParseConfig] = None)
	
	/**
	 * Reconfigure log presentation 
	 */
	def reindex(config: ParseConfig)
	
	/**
	 * Quit application
	 */
	def quit()
	
	/**
	 * Search next level entry 
	 */
	def scrollToLevel(level: LogLevel.LogLevel, toNext: Boolean, currentPos: Int)
	
	/**
	 * Search for text in log lines
	 */
	def searchText(text: Option[String], forward: Boolean, fromPos: Int)
	
	/**
	 * Perform reconfigure: Show reconfigure dialog and on an on...
	 */
	def reconfigure()
}


object StatusLineCell extends Enumeration {
	type StatusLineCell = Value
	
	val FILEPATH, TOTAL_LINES, CURRENT_LINE = Value
}

trait MainView extends WindowBase {
	import MainView._
	
	var presenter: MainViewPresenter = _
	
	def setAppTitle(subtitle: Option[String])
	
	def enableOpenedFileControls()
	
	def showReconfigure(config: ParseConfig)
	
	def cleanTable()
	def createTable(log: Log)
	
	def updateStatusLine(part: StatusLineCell.StatusLineCell, value: String)
	def showTextSearchStatus(found: SearchStatus.SearchStatus)
	
	def updateDetailView()
	
	def scrollTo(pos: Int)
}

object MainView {
	object SearchStatus extends Enumeration {
		type SearchStatus = Value
		
		val UNKNOWN, FOUND, NOT_FOUND = Value
	}
}
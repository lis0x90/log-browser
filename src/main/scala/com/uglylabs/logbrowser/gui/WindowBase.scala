package com.uglylabs.logbrowser.gui

import javafx.stage.Stage
import javafx.event.EventHandler
import javafx.stage.WindowEvent
import javafx.application.Platform

trait WindowBase {
	val userPrefPrefix = getClass.getName

	private var _stage: Stage = _
	def stage = _stage
	def stage_=(value: Stage): Unit = {
		_stage = value
		
		val closeHandler = new EventHandler[WindowEvent] {
			override def handle(event: WindowEvent): Unit = {
				UserPref.set(s"$userPrefPrefix.maximized", stage.isMaximized().toString)
				if(!stage.isMaximized()) {
					UserPref.set(s"$userPrefPrefix.width", stage.getWidth.toInt.toString)
					UserPref.set(s"$userPrefPrefix.height", stage.getHeight.toInt.toString)
				}
			}
		}

		// add close handler
		stage.onCloseRequestProperty().setValue(closeHandler)
		stage.onHidingProperty().setValue(closeHandler)

		// restore dialog size
		UserPref.get(s"$userPrefPrefix.width").map(v => stage.setWidth(v.toInt))
		UserPref.get(s"$userPrefPrefix.height").map(v => stage.setHeight(v.toInt))
		UserPref.get(s"$userPrefPrefix.maximized").map(v => stage.setMaximized(v.toBoolean))
	}
}

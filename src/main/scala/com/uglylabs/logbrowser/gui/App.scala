package com.uglylabs.logbrowser.gui

import javafx.application.{Application => FXApplication}
import javafx.stage.Stage
import scala.collection.JavaConverters._
import java.nio.file.Paths
import javafx.scene.image.Image
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.Parent
import org.slf4j.LoggerFactory
import javafx.application.Application.Parameters
import com.uglylabs.logbrowser.core.ParseConfig
import java.nio.charset.Charset
import javafx.application.Platform

class MainFrame extends FXApplication {
	private val log = LoggerFactory.getLogger(getClass)
	
	private val mainViewLoader = {
		val url = getClass.getResource("/forms/main.fxml").toURI.toURL
		new FXMLLoader(url)
	}

	val scene = new Scene(mainViewLoader.load().asInstanceOf[Parent])
	scene.getStylesheets().add("/styles.css")
	
	override def start(stage: Stage) {
		stage.setScene(scene)
		
		val view = mainViewLoader.getController[MainView]()
		view.stage = stage
		
		stage.getIcons().add(new Image("/icons/app-16.png"))
		stage.getIcons().add(new Image("/icons/app-24.png"))
		stage.getIcons().add(new Image("/icons/app-32.png"))
		stage.getIcons().add(new Image("/icons/app-48.png"))
		
		val main = new MainViewPresenterImpl(view)
		stage.show()
		processCommandLine(main, getParameters())
	}
	
	def processCommandLine(main: MainViewPresenter, params: Parameters) = {
		log.info("Handle command line params: " + params.getRaw)
		
		// TODO: show command line arguments
		if(params.getUnnamed().size == 0) {
			// nothing to do from command line 
		} else if(params.getUnnamed().size == 1) {
			val charset = Option(params.getNamed().get("charset"))
			val pattern = Option(params.getNamed().get("pattern"))
			val parseConfigOpt = if(pattern.isDefined) {
				Some(new ParseConfig(pattern.get, Charset.forName(charset.getOrElse("utf-8"))))
			} else {
				None
			}
			
			val logFile = Paths.get(params.getUnnamed().get(0)) 
			main.open(logFile, parseConfigOpt)
		} else {
			System.err.println("Invalid command line arguments: " + params.getRaw)
			System.err.println("""Usage: %prog% [--charset=<charset>] [--pattern=<pattern>] <logfile>""")
			main.quit()
			System.exit(1)
		}
	}
}

object App {
	def main(args: Array[String]) {
		Platform.setImplicitExit(true)
		FXUtils.fxPlatformStarted = true
		FXApplication.launch(classOf[MainFrame], args: _*)
	}
}
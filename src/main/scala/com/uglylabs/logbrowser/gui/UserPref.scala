package com.uglylabs.logbrowser.gui

import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.nio.file.Paths
import java.security.MessageDigest
import java.util.Properties

import org.slf4j.LoggerFactory

import com.uglylabs.logbrowser.core.ParseConfig

import ru.kamis.utils.commons.scala.CloseableHelper.using


object UserPref {
	private val log = LoggerFactory.getLogger(getClass)
	
	val PREF_DIR = System.getProperty("user.home") + "/.log-browser"
	
	def get(prop: String) = Option[String](config.get(prop).asInstanceOf[String])
	
	def set(prop: String, value: String) = {
		config.setProperty(prop, value)
		updateFile()
	}
	
	def delete(prop: String) = {
		config.remove(prop)
		updateFile()
	}
	
	def getParseConfig(filename: Path): Option[ParseConfig] = {
		val absPath = filename.toAbsolutePath.toString
		val key = "parseConfig." + sha1(absPath)
		get(key).map { filename =>
			log.info(s"Found parser configuration for: $absPath. Restore...")
			
			val pattern = get(s"$key.pattern").get
			val charset = Charset.forName(get(s"$key.charset").get)
			new ParseConfig(pattern, charset)
		}
	}
	
	def saveParseConfig(filename: Path, config: ParseConfig) {
		val absPath = filename.toAbsolutePath.toString
		val key = "parseConfig." + sha1(absPath)
		log.info(s"Save parser configuration for: $absPath, config: $config")
		set(key, absPath)
		set(s"$key.pattern", config.patternString)
		set(s"$key.charset", config.charset.name)
	}
	
	def sha1(str: String) = {
		val cript = MessageDigest.getInstance("SHA-1")
		cript.update(str.getBytes(StandardCharsets.UTF_8))
		cript.digest().map("%x".format(_)).mkString
	}
	
	// ====================================================
	// Private part
	// ====================================================
	
	private lazy val propsFile = {
		val path = Paths.get(s"$PREF_DIR/user-pref.conf")
		if(!path.toFile.isFile) {
			path.getParent.toFile.mkdirs()
			path.toFile.createNewFile()
		}
		path.toFile
	}  
	
	private lazy val config = {
		val props = new Properties()
		using(new FileInputStream(propsFile)) {
			props.load(_)
		}
		
		props
	}
	
	private def updateFile() = config.synchronized {
		using(new FileOutputStream(propsFile)) { out =>
			config.store(out, "Log Browser User Preferences File")
		}
	}
}
package com.uglylabs.logbrowser.gui

import java.util.List
import com.uglylabs.logbrowser.core.LogLine
import javafx.collections.ObservableList
import java.util.Collection
import java.util.ListIterator
import javafx.collections.ListChangeListener
import javafx.beans.InvalidationListener

class LogObservableWrapper(val underlying: Seq[LogLine]) extends List[LogLine] with ObservableList[LogLine] {
	private val _isEmpty = underlying.isEmpty
	private val _size = underlying.size
	
	def add(x$1: Int, x$2: LogLine): Unit = ???
	def add(x$1: LogLine): Boolean = ???
	def addAll(x$1: LogLine*): Boolean = ???
	def addAll(x$1: Int, x$2: Collection[_ <: com.uglylabs.logbrowser.core.LogLine]): Boolean = ???
	def clear(): Unit = ???
	def contains(x$1: Any): Boolean = ???
	def get(i: Int): LogLine = underlying(i)
	def indexOf(x: Any): Int = underlying.indexOf(x)
	def isEmpty(): Boolean = _isEmpty
	def listIterator(x$1: Int): ListIterator[LogLine] = ???
	def lastIndexOf(x$1: Any): Int = ???
	def remove(x$1: Int, x$2: Int): Unit = ???
	def containsAll(x$1: Collection[_]): Boolean = ???
	def remove(x$1: Int): LogLine = ???
	def iterator(): java.util.Iterator[LogLine] = ???
	def listIterator(): ListIterator[LogLine] = ???
	def set(x$1: Int, x$2: LogLine): LogLine = ???
	def size(): Int = _size
	def toArray(): Array[Object] = ???
	def toArray[T](x$1: Array[T with Object]): Array[T with Object] = ???
	def subList(x$1: Int, x$2: Int): List[LogLine] = ???
	def setAll(x$1: LogLine*): Boolean = ???
	def setAll(x$1: Collection[_ <: com.uglylabs.logbrowser.core.LogLine]): Boolean = ???
	def retainAll(x$1: Collection[_]): Boolean = ???
	def retainAll(x$1: LogLine*): Boolean = ???
	def removeAll(x$1: Collection[_]): Boolean = ???
	def removeAll(x$1: LogLine*): Boolean = ???
	def remove(x$1: Any): Boolean = ???
	def addAll(x$1: Collection[_ <: com.uglylabs.logbrowser.core.LogLine]): Boolean = ???

	
	def addListener(x$1: ListChangeListener[_ >: com.uglylabs.logbrowser.core.LogLine]): Unit = {
		// Ignore change listener because list is unmodifiable
	}

	def addListener(x$1: InvalidationListener): Unit = {
		// Ignore change listener because list is unmodifiable
	}

	def removeListener(x$1: InvalidationListener): Unit = {
		// Ignore change listener because list is unmodifiable
	}

	def removeListener(x$1: ListChangeListener[_ >: com.uglylabs.logbrowser.core.LogLine]): Unit = {
		// Ignore change listener because list is unmodifiable
	}
}
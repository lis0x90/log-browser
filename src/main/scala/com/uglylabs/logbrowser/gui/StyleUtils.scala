package com.uglylabs.logbrowser.gui

import javafx.scene.control.TextField

object StyleUtils {
	val textCorrect = "textCorrect"
	val textIncorrect = "textIncorrect"
	
	def cleanTextFieldStatus(field: TextField) = {
		field.getStyleClass().removeAll(textCorrect, textIncorrect)
	}
	
	def setTextFieldStatus(field: TextField, isCorrect: Boolean) = {
		val style = if (isCorrect) textCorrect else textIncorrect
		field.getStyleClass().add(style)
	}
}
package com.uglylabs.logbrowser.gui.dialogs

import java.io.File
import javafx.event.Event
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.Window
import javafx.stage.WindowEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Pane
import com.uglylabs.logbrowser.gui.UserPref
import com.uglylabs.logbrowser.gui.WindowBase

object DialogBuilder {
	def create[T <: DialogAbstract](
				owner: Window, 
				resource: String,
				dialogTitle: String = "Dialog",
				stageStyle: StageStyle = StageStyle.DECORATED): T = {
		
		val resourceFile = getClass.getResource(resource)
		require(resourceFile != null)
		val loader = new FXMLLoader(resourceFile.toURI().toURL())

		val stage = new Stage(stageStyle)
		stage.setTitle(dialogTitle)
		stage.initModality(Modality.APPLICATION_MODAL)
		stage.initOwner(owner)
		val rootLayout = loader.load().asInstanceOf[Parent]
		val scene = new Scene(rootLayout)
		val stylesheetPath = getClass.getResource("/styles.css").toExternalForm()
		scene.getStylesheets().add(stylesheetPath)
		stage.setScene(scene)

		// set minimum and maximum window size 
		val minWidth = rootLayout.asInstanceOf[Pane].getMinWidth()
		if (minWidth > 0) {
			stage.setMinWidth(minWidth)
		}
		val minHeight = rootLayout.asInstanceOf[Pane].getMinHeight()
		if (minHeight > 0) {
			stage.setMinHeight(minHeight)
		}

		// initialize controller and make cross references to presenter 
		val view = loader.getController[T]()
		require(view != null, s"Fxml misses controller class specification for: $resourceFile")
		view.stage = stage

		view
	}
}

protected trait DialogAbstract extends Initializable with WindowBase {
	var isSuccess = false

	def show() =
		stage.show()

	def showAndWait() =
		stage.showAndWait()

	def close() = {
		isSuccess = false
		stage.close()
	}

	def onClose() = close()

	def onSuccess() = {
		isSuccess = true
		stage.close()
	}
}
package com.uglylabs.logbrowser.gui.dialogs

import java.net.URL
import java.nio.file.Path
import java.util.ResourceBundle
import scala.collection.JavaConverters._
import com.uglylabs.logbrowser.core.ColumnTerm
import com.uglylabs.logbrowser.core.LogLine
import com.uglylabs.logbrowser.gui.FXUtils
import com.uglylabs.logbrowser.gui.FXUtils.{cellValueFactory => fxCellValueFactory}
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.stage.Window
import com.uglylabs.logbrowser.core.ParseConfig
import java.nio.charset.Charset
import com.uglylabs.logbrowser.core.ParseConfig
import com.uglylabs.logbrowser.core.ParseConfig
import com.uglylabs.logbrowser.core.ParserUtils
import javafx.scene.control.TextArea
import com.uglylabs.logbrowser.gui.StyleUtils
import com.uglylabs.logbrowser.core.Log
import javafx.scene.control.SplitPane
import javafx.stage.Stage
import com.uglylabs.logbrowser.gui.UserPref
import javafx.collections.ListChangeListener
import javafx.collections.ListChangeListener.Change
import javafx.scene.control.SplitPane.Divider
import javafx.stage.WindowEvent
import javafx.event.EventHandler
import javafx.application.Platform



object ReconfigureDialog {
	val LAYOUT = "/forms/reconfigure.fxml"
	
	def show(logfile: Path, config: ParseConfig)(implicit owner: Window): Option[ParseConfig] = {
		val controller = DialogBuilder.create[ReconfigureDialogView](owner, LAYOUT, dialogTitle = s"Configure: $logfile")
		
		controller.logfile = logfile
		controller.config = config
		
		controller.showAndWait()
		
		if(controller.isSuccess) {
			Some(controller.config)
		} else {
			None
		}
	}
}

class ReconfigureDialogView extends DialogAbstract {
	import FXUtils.{ 
		cellValueFactory => fxCellValueFactory,
		onChange => fxOnChange 
	}
	
	val sampleLogDataSize = Some(1024 * 20)
	
	@FXML var patternTextField: TextField = _
	@FXML var charsetTextField: TextField = _

	@FXML var patternParseTextArea: TextArea = _
	@FXML var detailsTextArea: TextArea = _
	
	@FXML var splitPane: SplitPane = _
	
	@FXML var logTableView: TableView[LogLine] = _
	
	private var logViewOpt: Option[Log] = None
	
	var logfile: Path = _
	
	override def initialize(url: URL, bundle: ResourceBundle) {
		require(patternTextField != null)
		require(charsetTextField != null)
		require(splitPane != null)
		require(patternParseTextArea != null)
		require(detailsTextArea != null)
		require(logTableView != null)
		
		patternTextField.textProperty().addListener(FXUtils.onChange[String]{ (_,_,_) =>
			refresh()
		})
		
		charsetTextField.textProperty().addListener(FXUtils.onChange[String]{ (_,_,_) =>
			refresh()
		})
		
		logTableView.getSelectionModel().selectedItemProperty().addListener(FXUtils.onChange[LogLine] { (_, _, next) =>
			val logLineOpt = Option(next)
			val text = logLineOpt.map { logLine =>
				logViewOpt.get.restoreLogText(logLine)
			}.getOrElse("")
			detailsTextArea.setText(text)
		})
	}
	
	override def stage_=(stage: Stage) = {
		super.stage_=(stage)
		
		FXUtils.insidePlatform {
        	UserPref.get(s"$userPrefPrefix.splits").map { values =>
				splitPane.setDividerPositions(values.split(",").map(_.toDouble): _*)
			}
			
			splitPane.getDividers().asScala.foreach { divider => 
				divider.positionProperty().addListener(FXUtils.onChange[Number] { (_, _, next) =>
					UserPref.set(s"$userPrefPrefix.splits", splitPane.getDividerPositions.mkString(","))
				})	
			}	 
		}
	}
	
	
	def config = {
		ParseConfig(patternTextField.getText, Charset.forName(charsetTextField.getText))
	}
	
	def config_=(c: ParseConfig) {
		patternTextField.setText(c.patternString)
		charsetTextField.setText(c.charset.name)
	}
	
	override def showAndWait() {
		refresh()
		super.showAndWait()
	}
	
	def refresh() = {
		StyleUtils.cleanTextFieldStatus(patternTextField)
		logViewOpt = None 
		cleanTable()
		try {
			import ParserUtils._
			implicit val _ = config.charset
			withLogIterator(logfile, config, sampleLogDataSize) { (parsers, logIterator) =>
				logViewOpt = Some(Log(parsers, logIterator.toList)) 
				reconfigureTable(parsers.columnTerms, logViewOpt.get.lines)
			}
			
			StyleUtils.setTextFieldStatus(patternTextField, true)
			patternParseTextArea.setText("Ok")
		} catch {
			case e: Throwable =>
				StyleUtils.setTextFieldStatus(patternTextField, false)
				patternParseTextArea.setText(e.toString)
		}
	}
	
	def cleanTable() {
		logTableView.getColumns().clear()
		detailsTextArea.setText("")
	}
	
	def reconfigureTable(columnTerms: Seq[ColumnTerm], lines: Seq[LogLine]) {
		// add rest option columns 
		columnTerms.foreach { columnTerm =>
			val tableColumn = new TableColumn[LogLine, String]()
			tableColumn.setText(columnTerm.columnName)
			tableColumn.setCellValueFactory(fxCellValueFactory[LogLine, String] { cell =>
				val col = cell.getTableColumn()
				val pos = cell.getTableView().getColumns().indexOf(col)
				val logLine = cell.getValue()
				new SimpleStringProperty(logLine.items(pos))
			})

			logTableView.getColumns().add(tableColumn)
		}
		
		logTableView.setItems(FXCollections.observableArrayList(lines.asJava))
		if(!lines.isEmpty) {
			// select first line 
			FXUtils.insidePlatform {
				logTableView.getSelectionModel.select(0)
				logTableView.getFocusModel.focus(0)
			}
		}
	}
}

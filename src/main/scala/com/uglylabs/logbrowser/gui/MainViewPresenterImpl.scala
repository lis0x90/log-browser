package com.uglylabs.logbrowser.gui

import java.nio.file.Path

import org.slf4j.LoggerFactory

import com.uglylabs.logbrowser.core.Log
import com.uglylabs.logbrowser.core.LogLevel
import com.uglylabs.logbrowser.core.ParseConfig
import com.uglylabs.logbrowser.core.ParserUtils
import com.uglylabs.logbrowser.gui.MainView.SearchStatus

object MainViewPresenterImpl {
	private val log = LoggerFactory.getLogger(getClass)
}

class MainViewPresenterImpl(override val view: MainView) extends MainViewPresenter {
	import MainViewPresenterImpl._
	
	view.presenter = this
	
	override var logFileOpt: Option[Path] = None
	var logOpt: Option[Log] = None
	var parseConfig: ParseConfig = new ParseConfig()
 	
	override def open(logFile: Path, configOpt: Option[ParseConfig] = None) {
		log.info(s"User select log file: $logFile")
		
		// suggest config for selected file
		parseConfig = UserPref.getParseConfig(logFile).getOrElse(new ParseConfig())
		
		logFileOpt = Some(logFile)
		view.updateStatusLine(StatusLineCell.FILEPATH, s"Log file: $logFile")
		view.setAppTitle(Some(logFile.toString))
		
		configOpt.map(reindex(_)).getOrElse(reconfigure())
	} 
	
	override def reconfigure() {
		view.showReconfigure(parseConfig)
	} 
	
	override def reindex(config: ParseConfig) {
		// save new parse config
		parseConfig = config
		
		import scala.concurrent._
		import scala.concurrent.ExecutionContext.Implicits._
		
		Future {
			logFileOpt.map { filepath =>
				UserPref.saveParseConfig(filepath, config)
			
				log.info(s"Reindex $filepath with pattern: $config")
						
				view.cleanTable()
				log.info("Create in-memory collection...")
				
				var logCount = 0
				val logData = ParserUtils.parse(filepath, config) { logLine => 
					logCount += 1
					// on update on 1000 lines 
					if(logCount % 1000 == 0) {
						FXUtils.insidePlatform {
							view.updateStatusLine(StatusLineCell.TOTAL_LINES, s"Total: $logCount")
						}
					}
				}
				
				log.info(s"Created collection of: $logCount")
				FXUtils.insidePlatform {
					view.updateStatusLine(StatusLineCell.TOTAL_LINES, s"Total: $logCount")
					view.createTable(logData)
					view.enableOpenedFileControls()
				}
				
				logOpt = Some(logData)
			}
		}.onFailure {
			// TODO handle exception properly!
			case e =>
				log.error("Error during future run", e)
				throw e 
		}
	}
	
	case class ForBreak(i: Int) extends Exception
	
	override def searchText(textOpt: Option[String], forward: Boolean, fromPos: Int) = {
		log.info(s"Search for: `$textOpt' to ${if(forward) "forward" else "backward"} from pos: $fromPos")
		view.showTextSearchStatus(SearchStatus.UNKNOWN)
		textOpt.map { text =>
			logOpt.map { logView => 
				try {
					val (start, finish, step) = calcTextSearchBounds(fromPos, forward)
					log.debug(s"Search bounds: $start -> $finish (step: $step)")
					for(i <- start.until(finish, step)) {
						val logLine = logView.lines(i)
						
						logLine.items.find(_.indexOf(text) >= 0).map { _ =>
							throw new ForBreak(i)
						}
						
						if(logLine.tail.indexOf(text) >= 0) {
							throw new ForBreak(i)
						}
					}
					
					view.showTextSearchStatus(SearchStatus.NOT_FOUND)
				} catch {
					case ForBreak(pos) =>
						log.info(s"Found text at: $pos")
						view.showTextSearchStatus(SearchStatus.FOUND)
						view.scrollTo(pos)
	    		}
			}
		}
		view.updateDetailView()
	}
	
	override def scrollToLevel(level: LogLevel.LogLevel, forward: Boolean, fromPos: Int) {
		log.info(s"Search for $level ${if(forward) "forward" else "backward"} from $fromPos")
		logOpt.map { logData =>
			logData.levelColumnIndexOpt.map { _ => 
				try {
					val (start, finish, step) = calcLevelSearchBounds(fromPos, forward)
					log.debug(s"Search from: $start to $finish")
					
					for(i <- start.until(finish, step)) {
						val curLineLevel = logData.getLineLevel(i)
						if(curLineLevel.isDefined && curLineLevel.get == level) {
							throw new ForBreak(i)
						}
					}
					
					// nothing found
					log.info(s"Level not found")
				} catch {
					case ForBreak(pos) =>
						view.scrollTo(pos)
				}
			}
		}
	}
	
	override def quit() = {
		log.info("Quit program")
	}
	
	def calcTextSearchBounds(fromPos: Int, forward: Boolean): (Int, Int, Int) = {
		logOpt.map { logData =>
			if(forward) {
				(
					// if cursor at the end of line in table and we need to search from start
					if(fromPos > logData.lines.size -1) 0 else fromPos, 
					logData.lines.size, 
					1
				)
			} else {
				(
					// if cursor at the top of table,  start search from end 
					if(fromPos < 0) logData.lines.size - 1 else fromPos, 
					-1, 
					-1
				)
			}
		}.getOrElse(0, 0, 1)
	}
	
	def calcLevelSearchBounds(fromPos: Int, forward: Boolean): (Int, Int, Int) = {
		logOpt.map { logData =>
			if(forward) {
				(
					// if cursor at the end of line in table and we need to search from start
					if(fromPos >= logData.lines.size -1) 0 else fromPos + 1, 
					logData.lines.size, 
					1
				)
			} else {
				(
					// if cursor at the top of table,  start search from end 
					if(fromPos <= 0) logData.lines.size - 1 else fromPos - 1, 
					0, 
					-1
				)
			}
		}.getOrElse(0, 0, 1)
	}
}
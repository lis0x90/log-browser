package com.uglylabs.logbrowser.gui

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import com.uglylabs.logbrowser.core.ColumnTerm
import com.uglylabs.logbrowser.core.LogLineIterator
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser
import javafx.application.Platform
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.scene.control.TableColumn
import javafx.util.Callback
import ru.kamis.utils.commons.scala.CloseableHelper.using
import com.uglylabs.logbrowser.core.ParseConfig
import javafx.collections.ListChangeListener

object FXUtils {
	def onChange[T](f: (ObservableValue[_ <: T], T, T) => Unit) = new ChangeListener[T]() {
		override def changed(v: ObservableValue[_ <: T], prev: T, next: T) {
			f(v, prev, next)
		}
	}
	
	def cellValueFactory[T, Y](f : (TableColumn.CellDataFeatures[T, Y]) => ObservableValue[Y]) = 
		new Callback[TableColumn.CellDataFeatures[T, Y], ObservableValue[Y]] {
			override def call(cell: TableColumn.CellDataFeatures[T, Y]): ObservableValue[Y] = f(cell)
		}
	
	// JavaFX tricks
	
	// its wariable need for correct test passing in headless mode 
	var fxPlatformStarted = false
	def insidePlatform(f : => Unit) {
		if(fxPlatformStarted) {
			// platform is started and we need to play by its rules
			Platform.runLater(new Runnable {
				override def run(): Unit = f
			})
		} else {
			// headless mode, just run required code
			f
		}
	}
}
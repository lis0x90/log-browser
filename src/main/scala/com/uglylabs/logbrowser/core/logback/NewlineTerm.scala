package com.uglylabs.logbrowser.core.logback

import com.uglylabs.logbrowser.core.IgnoreTerm

/**
 * @author s.lisovoy
 */
case class NewlineTerm() extends IgnoreTerm {
	// its indicates that it should be ended with newline  
	override def createPattern() = "(?:\r|\n|\r\n)"
	override def restorePart() = "\n"
}

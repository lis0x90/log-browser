package com.uglylabs.logbrowser.core.logback

import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers

import com.uglylabs.logbrowser.core.Align
import com.uglylabs.logbrowser.core.LogFormatTerm
import com.uglylabs.logbrowser.core.LogParser

object LogbackPatternParser extends RegexParsers {

	override def skipWhitespace = false

	def positiveNumber: Parser[Int] = """\d+""".r ^^ { _.toInt }
	def number: Parser[Int] = """-?\d+""".r ^^ { _.toInt }
	def modifierTerm: Parser[Int] = "{" ~> positiveNumber <~ "}"

	// formatting aligment
	private def alignFullTerm: Parser[Align] = (number <~ ".") ~ number ^^ { case left ~ right => Align(Some(left), Some(right)) }
	private def alignLeftTerm: Parser[Align] = number <~ opt(".") ^^ { case left => Align(Some(left), None) }
	private def alignRightTerm: Parser[Align] = "." ~> number ^^ { case right => Align(None, Some(right)) }
	def alignTerm: Parser[Align] = alignFullTerm | alignLeftTerm | alignRightTerm

	// template for all terms 
	def itemTerm(spec: String)(f: (Option[Align], Option[Int]) => LogFormatTerm): Parser[LogFormatTerm] =
		literal("%") ~> opt(alignTerm) ~ (new Regex(spec) ~> opt(modifierTerm)) ^^ { case align ~ length => f(align, length) }

	def loggerTerm = itemTerm("c|lo|logger")(LoggerTerm)

	def classTerm = itemTerm("C|class")(ClassTerm)

	def contextNameTerm = itemTerm("contextName|cn")(ContextNameTerm)

	def levelTerm = itemTerm("p|le|level")(LevelTerm)

	def threadTerm = itemTerm("t|thread")(ThreadTerm)

	// s - jboss message identifier
	def messageTerm = itemTerm("m|msg|message|s")(MessageTerm)
	
	// E - jboss message identifier
	def exceptionTerm = itemTerm("E|ex|exception|throwable")(ExceptionTerm)

	// timezone support is not yet implemented
	def dateFormatPattern: Parser[String] = "{" ~> """[^}]+""".r <~ "}"
	def dateTerm: Parser[DateTerm] = """%(d|date)""".r ~> opt(dateFormatPattern) ^^ { fmt => DateTerm(fmt.getOrElse("yyyy-MM-dd HH:mm:ss,SSS")) }

	def newlineTerm: Parser[NewlineTerm] = "%n".r ^^^ { NewlineTerm() }

	def anyCharTerm: Parser[LogFormatTerm] = ".".r ^^ { CharTerm(_) }

	def lineParser: Parser[Seq[LogFormatTerm]] = rep(loggerTerm
		| classTerm
		| contextNameTerm
		| dateTerm
		| levelTerm
		| threadTerm
		| exceptionTerm
		| newlineTerm
		| messageTerm
		| anyCharTerm)

	def apply(pattern: String) = parseAll(lineParser, pattern) match {
		case Success(result, _) =>
			new LogParser(reduceCharTerms(result))

		case e: NoSuccess =>
			throw new IllegalArgumentException(s"Error parse input: $pattern\n" + e)
	}

	def reduceCharTerms(list: Seq[LogFormatTerm]) = {
		val buf = new ListBuffer[LogFormatTerm]()
		list.foreach { _ match {
				case p: CharTerm =>
					if(!buf.isEmpty && buf.last.isInstanceOf[CharTerm]) {
						val last = buf.remove(buf.size - 1).asInstanceOf[CharTerm]
						buf += CharTerm(last.char + p.char)
					} else {
						buf += p
					}
				case p => 
					buf += p
			}
		}
		
		//last term always should be an %n 
		// add them if it missing
		if(!buf.last.isInstanceOf[NewlineTerm])
			buf += new NewlineTerm
				
		buf
	}
}

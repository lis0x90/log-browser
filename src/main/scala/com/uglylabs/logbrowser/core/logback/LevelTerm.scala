package com.uglylabs.logbrowser.core.logback

import com.uglylabs.logbrowser.core.ColumnTerm
import com.uglylabs.logbrowser.core.Align
import com.uglylabs.logbrowser.core.LogLevel

/**
 * @author s.lisovoy
 */
case class LevelTerm(
		override val alignOpt: Option[Align],
		override val length: Option[Int] = None) extends ColumnTerm(alignOpt, length) {

	override val columnName = "Level"

	override def createPattern() = {
		val pattern = LogLevel.values.map { level => // align it first
			restorePart().format(level.toString)
		}.mkString("|")

		// surround with group
		s"($pattern)"
	}

	override def map(value: String) = value.trim()

	// TODO: may be use specific level?
	override def emptyValue = LogLevel.TRACE.toString
}

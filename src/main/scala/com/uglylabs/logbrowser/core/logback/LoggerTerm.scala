package com.uglylabs.logbrowser.core.logback

import com.uglylabs.logbrowser.core.ColumnTerm
import com.uglylabs.logbrowser.core.Align
import com.uglylabs.logbrowser.core.ColumnTerm


/**
 * @author s.lisovoy
 */
case class LoggerTerm(
			override val alignOpt: Option[Align], 
			override val length: Option[Int] = None
		) extends ColumnTerm(alignOpt, length) {
	
	override val columnName = "Logger"
}

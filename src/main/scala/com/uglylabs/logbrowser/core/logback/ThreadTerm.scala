package com.uglylabs.logbrowser.core.logback

import com.uglylabs.logbrowser.core.ColumnTerm
import com.uglylabs.logbrowser.core.Align

/**
 * @author s.lisovoy
 */
case class ThreadTerm(
		override val alignOpt: Option[Align],
		override val length: Option[Int] = None) extends ColumnTerm(alignOpt, length) {

	override val columnName = "Thread"
} 
package com.uglylabs.logbrowser.core.logback

import java.util.regex.Pattern
import com.uglylabs.logbrowser.core.IgnoreTerm

/**
 * @author s.lisovoy
 */
case class CharTerm(char: String) extends IgnoreTerm {
	override def createPattern() = Pattern.quote(char)

	override def restorePart() = char.replace("%", "%%")
}

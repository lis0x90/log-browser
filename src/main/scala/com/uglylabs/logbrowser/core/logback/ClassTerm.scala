package com.uglylabs.logbrowser.core.logback

import com.uglylabs.logbrowser.core.ColumnTerm
import com.uglylabs.logbrowser.core.Align

case class ClassTerm(
		override val alignOpt: Option[Align],
		override val length: Option[Int] = None) extends ColumnTerm(alignOpt, length) {

	override val columnName = "Class"
} 
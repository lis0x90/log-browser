package com.uglylabs.logbrowser.core.logback

import com.uglylabs.logbrowser.core.ColumnTerm
import java.text.DateFormat
import scala.util.parsing.combinator.RegexParsers
import java.time.DayOfWeek
import java.util.regex.Pattern
import java.text.SimpleDateFormat


/**
 * @author s.lisovoy
 */
case class DateTerm(format: String) extends ColumnTerm(None, None) {
	override val columnName = "Date"
	override def createPattern() = "(" + SimpleDateFormatParser.createPattern(format) + ")"
	
	private lazy val dateFormatter = new SimpleDateFormat(format)
	def parseTime(s: String) = dateFormatter.parse(s)
}

private[logback] object SimpleDateFormatParser extends RegexParsers {
	import date._
	
	override def skipWhitespace = false
	
	def year: Parser[DateTermPart] = """[yY]+""".r ^^ { YearTerm } // year and week year 
	def month: Parser[DateTermPart] = """M+""".r ^^ { MonthTerm }
	def dayInMonth: Parser[DateTermPart] = """d+""".r ^^ { DayInMonthTerm }
	def hourInDay: Parser[DateTermPart] = """H+""".r ^^ { HourInDayTerm }
	def minuteInHour: Parser[DateTermPart] = """m+""".r ^^ { MinuteInHourTerm }
	def secondInMinute: Parser[DateTermPart] = """s+""".r ^^ { SecondInMinuteTerm }
	def millisecond: Parser[DateTermPart] = """S+""".r ^^ { MillisecondTerm }
	
	def chars: Parser[DateTermPart] = ("""[^'\w]""".r | "'.+?'".r) ^^ { CharsTerm }  
	  
	
	def allTerms = rep(year 
			| month 
			| dayInMonth 
			| hourInDay 
			| minuteInHour 
			| secondInMinute 
			| millisecond 
			| chars)
			
	def createPattern(pattern: String) = parseAll(allTerms, pattern) match {
		case Success(result, _) =>
			result.map(_.pattern).mkString

		case e: NoSuccess =>
			throw new IllegalArgumentException(s"Error parse input: $pattern\n" + e)
	}
}


package date {
	trait DateTermPart {
		def pattern: String
	}

	case class CharsTerm(what: String) extends DateTermPart {
		override val pattern: String = Pattern.quote(what)
	}
	
	case class YearTerm(format: String) extends DateTermPart {
		override val pattern = format.length match {
			case 1 | 3 => """\d{4}"""
			case 2 => """\d{2}"""
			case _ => s"\\d{${format.length}}"
		}
		}
	
	case class MonthTerm(format: String) extends DateTermPart {
		override val pattern = format.length match {
			case 1 => """\d{1,2}"""
			case 2 => """\d{2}"""
			case _ => """\w+"""
		}
	}
	
	private[date] abstract class NumberSequenceTerm(val capacity: Int) extends DateTermPart {
		this: { def format: String } =>
		
		override val pattern = {
			val align = format.length
			if(align < capacity) 
				s"\\d{$align,$capacity}" 
			else 
				s"\\d{$align}"
		}
	}
	
	case class DayInMonthTerm(format: String) extends NumberSequenceTerm(2)
	case class HourInDayTerm(format: String) extends NumberSequenceTerm(2)
	case class MinuteInHourTerm(format: String) extends NumberSequenceTerm(2)
	case class SecondInMinuteTerm(format: String) extends NumberSequenceTerm(2)
	case class MillisecondTerm(format: String) extends NumberSequenceTerm(3)
}
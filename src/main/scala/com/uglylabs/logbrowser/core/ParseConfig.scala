package com.uglylabs.logbrowser.core

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

case class ParseConfig(
		patternString: String = "%s%n", 
		charset: Charset = StandardCharsets.UTF_8
	)
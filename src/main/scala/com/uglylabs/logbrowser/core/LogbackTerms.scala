package com.uglylabs.logbrowser.core

import java.util.regex.Pattern
import java.text.DateFormat

trait LogFormatTerm {
	def createPattern(): String
	def restorePart(): String
}

trait IgnoreTerm extends LogFormatTerm

abstract class ColumnTerm(val alignOpt: Option[Align], val length: Option[Int] = None) extends LogFormatTerm {
	
	/**
	 * Column name for table 
	 */
	def columnName: String

	/**
	 * Map found values to real terms 
	 */
	def map(term: String) = term
	
	/**
	 * Default pattern for catched terms  
	 */
	def createPattern(): String = "(.+?)"
	
	/**
	 * Common template to restore log line by value 
	 * TODO: also need apply alignments 
	 */
	override def restorePart() = alignOpt.map { align =>
			val leftMod = align.left.getOrElse("")
			val rightMod = align.right.map("." + _).getOrElse("")
			
			s"%${leftMod}${rightMod}s"
		}.getOrElse("%s")
		
	/**
	 * Used to create empty line to attach found message tail 
	 */
	def emptyValue: String = " -- "
}

case class Align(left: Option[Int], right: Option[Int])  

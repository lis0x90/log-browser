package com.uglylabs.logbrowser.core

import java.io.BufferedReader
import java.io.Reader
import scala.util.Left
import com.uglylabs.logbrowser.core.logback.NewlineTerm

object LogLineIterator {
	def apply(in: Iterator[String], parser: LogParser) = {
		val linesCount = parser.allTerms.filter(_.isInstanceOf[NewlineTerm]).size
		
		// intentionally do not create Option object to speed up processing
		val stream = Stream.continually {
						in.take(linesCount)
					}.takeWhile {
						!_.isEmpty	
					}.map { lines =>
						// restore input to requested chunk newline delimited format 
						val chunk = lines.mkString("\n") + "\n" // TODO externalize newline char
						// parse readed lines chunk
						parser.parse(chunk)
					}

		new LogLineIterator(parser, stream.toIterator)
	}
}

class LogLineIterator(
		private val parser: LogParser, 
		private val stream: Iterator[Either[Array[String], String]]
		) extends Iterator[LogLine] {
	
	private var index: Int = -1
	private def nextIndex = { index +=1; index } 
	private var nextLogLine: Option[LogLine] = None
	
	override def hasNext() = {
		if(nextLogLine.isDefined) {
			true
		} else {
			stream.hasNext
		}
	}
	
	override def next(): LogLine = {
		val currentLine = nextLogLine.getOrElse {
			// read next line
			stream.next() match {
				case Left(logLineChunks) =>
					LogLine(nextIndex, logLineChunks)
				case Right(tailLine) =>
					// we found tail without headed message
					// create log line with empty values
					val line = LogLine(nextIndex, parser.columnTerms.map(_.emptyValue).toArray)
					line.tail += tailLine
					line
			}
		}
		
		nextLogLine = None
		while(stream.hasNext && nextLogLine.isEmpty) {
			stream.next() match {
				case Right(e) =>
					currentLine.tail += e
				case Left(e) =>
					nextLogLine = Some(LogLine(nextIndex, e))
			}
		}
		
		currentLine
	}
}
package com.uglylabs.logbrowser.core

import scala.collection.mutable.ListBuffer

case class LogLine(
		val index: Int, 
		val items: Array[String], 
		var tail: ListBuffer[String] = new ListBuffer()) 

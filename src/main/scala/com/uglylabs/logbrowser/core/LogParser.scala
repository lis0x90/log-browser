package com.uglylabs.logbrowser.core

import java.text.DateFormat
import java.text.SimpleDateFormat
import scala.util.parsing.combinator.RegexParsers
import scala.util.matching.Regex
import scala.collection.mutable.ListBuffer
import java.util.regex.Pattern
import scala.util.Success
import scala.util.Either
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.file.Files
import java.io.Reader
import java.io.BufferedReader

case class LogParser(val allTerms: Seq[LogFormatTerm]) {
	import LogParser._
	
	private lazy val pattern = Pattern.compile(createPattern(), Pattern.MULTILINE)
	
	val columnTerms = allTerms
				.filter(_.isInstanceOf[ColumnTerm])
				.map(_.asInstanceOf[ColumnTerm])
	
	def parse(input: String): Either[Array[String], String] = {
		val m = pattern.matcher(input)
		if(m.matches()) {
			val values = (0 until m.groupCount()).map { i => 
					val value = m.group(i + 1);
					columnTerms(i).map(value)
				}.toArray
			Left(values)
		} else {
			Right(input)
		}
	}
	
	def createPattern() = {
		allTerms.map(_.createPattern()).reduceLeft(_ + _)
	}	
}
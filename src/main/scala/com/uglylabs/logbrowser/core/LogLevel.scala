package com.uglylabs.logbrowser.core

/**
 * @author s.lisovoy
 */
object LogLevel extends Enumeration {
	type LogLevel = Value

	val TRACE = Value(10)
	val DEBUG = Value(20)
	val INFO = Value(30)
	val WARN = Value(40)
	val ERROR = Value(50)
	val FATAL = Value(60)
}
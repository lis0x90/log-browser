package com.uglylabs.logbrowser.core

import com.uglylabs.logbrowser.core.logback.LevelTerm
import com.uglylabs.logbrowser.core.logback.DateTerm
import ch.qos.logback.classic.db.names.ColumnName
import com.uglylabs.logbrowser.core.logback.DateTerm
import com.uglylabs.logbrowser.core.logback.DateTerm
import com.uglylabs.logbrowser.core.logback.DateTerm

case class Log(private val parser: LogParser, lines: Seq[LogLine]) {
	val levelColumnIndexOpt = 
		parser.columnTerms.zipWithIndex.find(_._1.isInstanceOf[LevelTerm]).map(_._2)

	val columns: Seq[ColumnTerm] = parser.columnTerms
	
	private lazy val dateColumnIndexOpt = 
		parser.columnTerms.zipWithIndex.find(_._1.isInstanceOf[DateTerm]).map(_._2)
		
	lazy val maxDuration = durationColumn.map(_._2.max)
	
	lazy val durationColumn : Option[(Int, Array[Long])] = {
		dateColumnIndexOpt.map { dateColumnIndex => 
			var prevTimestamp = Long.MinValue
			val dateParser = columns(dateColumnIndex).asInstanceOf[DateTerm].parseTime _
			val data = new Array[Long](lines.size)
			
			(0 until lines.size).foreach { i =>
				val dateString = lines(i).items(dateColumnIndex)
				val timestamp = dateParser(dateString).getTime
				
				if(i == 0) {
					// skip first iteration because prevTimestamp not yet initialized  
				} else {
					data(i - 1) = timestamp - prevTimestamp 
				}
				
				prevTimestamp = timestamp 
			}
					
			(dateColumnIndex, data)
		}
	}
	
	def getLineLevel(lineNumber: Int): Option[LogLevel.LogLevel] = 
		levelColumnIndexOpt.map { pos => 
			LogLevel.withName(lines(lineNumber).items(pos)) 
		}
		
	def restoreLogText(line: LogLine) = 
		restoreFormat.format(line.items:_*) + line.tail.mkString

	private val restoreFormat = 
		parser.allTerms.map(_.restorePart()).mkString
}
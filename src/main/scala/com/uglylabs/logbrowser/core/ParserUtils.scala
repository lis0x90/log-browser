package com.uglylabs.logbrowser.core

import java.io.StringReader
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import scala.collection.mutable.ArrayBuffer
import ru.kamis.utils.commons.scala.CloseableHelper._
import java.nio.ByteBuffer
import com.uglylabs.logbrowser.core.logback.LogbackPatternParser
import scala.io.Source
import scala.io.Codec

object ParserUtils {
	def withLogIterator[T](logfile: Path, config: ParseConfig, limit: Option[Int] = None)
			(processor: (LogParser, LogLineIterator) => T): T = {
		
		val logParser = LogbackPatternParser(config.patternString)
		
		implicit val _ = Codec(config.charset)
		using(Source.fromFile(logfile.toFile)) { reader =>
			val lines = limit.map(reader.getLines.take(_)).getOrElse(reader.getLines)
			val it = LogLineIterator(lines, logParser)
			processor(logParser, it)
		}
	}
	
	def parse(logfile: Path, config: ParseConfig)(lineCallback: LogLine => Unit): Log  = {
		withLogIterator(logfile, config) { (parser, it) =>
			val lines = new ArrayBuffer[LogLine]()
			
			it.foreach { logLine => 
				lineCallback(logLine)
				lines += logLine
			}
			
			Log(parser, lines)
		}
	}
}